package opendota.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import skadistats.clarity.wire.s2.proto.S2DotaGcCommon;

@Generated("com.robohorse.robopojogenerator")
public class PicksBans{

	public PicksBans(S2DotaGcCommon.CMatchHeroSelectEvent heroSelectEvent, int order){
		this.heroId = heroSelectEvent.getHeroId();
		this.isPick = heroSelectEvent.getIsPick();
		this.order = order;
		this.team = heroSelectEvent.getTeam();
	}

	@SerializedName("hero_id")
	private int heroId;

	@SerializedName("team")
	private int team;

	@SerializedName("is_pick")
	private boolean isPick;

	@SerializedName("order")
	private int order;

	public void setHeroId(int heroId){
		this.heroId = heroId;
	}

	public int getHeroId(){
		return heroId;
	}

	public void setTeam(int team){
		this.team = team;
	}

	public int getTeam(){
		return team;
	}

	public void setIsPick(boolean isPick){
		this.isPick = isPick;
	}

	public boolean isIsPick(){
		return isPick;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"PicksBansItem{" + 
			"hero_id = '" + heroId + '\'' + 
			",team = '" + team + '\'' + 
			",is_pick = '" + isPick + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}