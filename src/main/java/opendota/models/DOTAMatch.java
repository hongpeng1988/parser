package opendota.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import opendota.models.Player;
import opendota.models.PicksBans;
import opendota.models.AbilityUpgrade;

public class DOTAMatch{
    public final String type = "CMsgDOTAMatch";

	@SerializedName("first_blood_time")
	private int firstBloodTime;

	@SerializedName("cluster")
	private int cluster;

	@SerializedName("match_seq_num")
	private String matchSeqNum;

	@SerializedName("flags")
	private int flags;

	@SerializedName("pre_game_duration")
	private int preGameDuration;

	@SerializedName("human_players")
	private int humanPlayers;

	@SerializedName("barracks_status_radiant")
	private int barracksStatusRadiant;

	@SerializedName("duration")
	private int duration;

	@SerializedName("radiant_win")
	private boolean radiantWin;

	@SerializedName("radiant_logo")
	private String radiantLogo;

	@SerializedName("engine")
	private int engine;

	@SerializedName("dire_name")
	private String direName;

	@SerializedName("negative_votes")
	private int negativeVotes;

	@SerializedName("radiant_captain")
	private int radiantCaptain;

	@SerializedName("picks_bans")
	private List<PicksBans> picksBans;

	@SerializedName("dire_team_complete")
	private int direTeamComplete;

	@SerializedName("dire_team_id")
	private int direTeamId;

	@SerializedName("tower_status_radiant")
	private int towerStatusRadiant;

	@SerializedName("players")
	private List<Player> players;

	@SerializedName("match_id")
	private Long matchId;

	@SerializedName("lobby_type")
	private int lobbyType;

	@SerializedName("radiant_team_complete")
	private int radiantTeamComplete;

	@SerializedName("start_time")
	private int startTime;

	@SerializedName("radiant_name")
	private String radiantName;

	@SerializedName("leagueid")
	private int leagueid;

	@SerializedName("dire_logo")
	private String direLogo;

	@SerializedName("dire_score")
	private int direScore;

	@SerializedName("dire_captain")
	private int direCaptain;

	@SerializedName("tower_status_dire")
	private int towerStatusDire;

	@SerializedName("positive_votes")
	private int positiveVotes;

	@SerializedName("game_mode")
	private int gameMode;

	@SerializedName("radiant_score")
	private int radiantScore;

	@SerializedName("radiant_team_id")
	private int radiantTeamId;

	@SerializedName("barracks_status_dire")
	private int barracksStatusDire;

	public void setFirstBloodTime(int firstBloodTime){
		this.firstBloodTime = firstBloodTime;
	}

	public int getFirstBloodTime(){
		return firstBloodTime;
	}

	public void setCluster(int cluster){
		this.cluster = cluster;
	}

	public int getCluster(){
		return cluster;
	}

	public void setMatchSeqNum(String matchSeqNum){
		this.matchSeqNum = matchSeqNum;
	}

	public String getMatchSeqNum(){
		return matchSeqNum;
	}

	public void setFlags(int flags){
		this.flags = flags;
	}

	public int getFlags(){
		return flags;
	}

	public void setPreGameDuration(int preGameDuration){
		this.preGameDuration = preGameDuration;
	}

	public int getPreGameDuration(){
		return preGameDuration;
	}

	public void setHumanPlayers(int humanPlayers){
		this.humanPlayers = humanPlayers;
	}

	public int getHumanPlayers(){
		return humanPlayers;
	}

	public void setBarracksStatusRadiant(int barracksStatusRadiant){
		this.barracksStatusRadiant = barracksStatusRadiant;
	}

	public int getBarracksStatusRadiant(){
		return barracksStatusRadiant;
	}

	public void setDuration(int duration){
		this.duration = duration;
	}

	public int getDuration(){
		return duration;
	}

	public void setRadiantWin(boolean radiantWin){
		this.radiantWin = radiantWin;
	}

	public boolean isRadiantWin(){
		return radiantWin;
	}

	public void setRadiantLogo(String radiantLogo){
		this.radiantLogo = radiantLogo;
	}

	public String getRadiantLogo(){
		return radiantLogo;
	}

	public void setEngine(int engine){
		this.engine = engine;
	}

	public int getEngine(){
		return engine;
	}

	public void setDireName(String direName){
		this.direName = direName;
	}

	public String getDireName(){
		return direName;
	}

	public void setNegativeVotes(int negativeVotes){
		this.negativeVotes = negativeVotes;
	}

	public int getNegativeVotes(){
		return negativeVotes;
	}

	public void setRadiantCaptain(int radiantCaptain){
		this.radiantCaptain = radiantCaptain;
	}

	public int getRadiantCaptain(){
		return radiantCaptain;
	}

	public void setPicksBans(List<PicksBans> picksBans){
		this.picksBans = picksBans;
	}

	public List<PicksBans> getPicksBans(){
		return picksBans;
	}

	public void setDireTeamComplete(int direTeamComplete){
		this.direTeamComplete = direTeamComplete;
	}

	public int getDireTeamComplete(){
		return direTeamComplete;
	}

	public void setDireTeamId(int direTeamId){
		this.direTeamId = direTeamId;
	}

	public int getDireTeamId(){
		return direTeamId;
	}

	public void setTowerStatusRadiant(int towerStatusRadiant){
		this.towerStatusRadiant = towerStatusRadiant;
	}

	public int getTowerStatusRadiant(){
		return towerStatusRadiant;
	}

	public void setPlayers(List<Player> players){
		this.players = players;
	}

	public List<Player> getPlayers(){
		return players;
	}

	public void setMatchId(Long matchId){
		this.matchId = matchId;
	}

	public Long getMatchId(){
		return matchId;
	}

	public void setLobbyType(int lobbyType){
		this.lobbyType = lobbyType;
	}

	public int getLobbyType(){
		return lobbyType;
	}

	public void setRadiantTeamComplete(int radiantTeamComplete){
		this.radiantTeamComplete = radiantTeamComplete;
	}

	public int getRadiantTeamComplete(){
		return radiantTeamComplete;
	}

	public void setStartTime(int startTime){
		this.startTime = startTime;
	}

	public int getStartTime(){
		return startTime;
	}

	public void setRadiantName(String radiantName){
		this.radiantName = radiantName;
	}

	public String getRadiantName(){
		return radiantName;
	}

	public void setLeagueid(int leagueid){
		this.leagueid = leagueid;
	}

	public int getLeagueid(){
		return leagueid;
	}

	public void setDireLogo(String direLogo){
		this.direLogo = direLogo;
	}

	public String getDireLogo(){
		return direLogo;
	}

	public void setDireScore(int direScore){
		this.direScore = direScore;
	}

	public int getDireScore(){
		return direScore;
	}

	public void setDireCaptain(int direCaptain){
		this.direCaptain = direCaptain;
	}

	public int getDireCaptain(){
		return direCaptain;
	}

	public void setTowerStatusDire(int towerStatusDire){
		this.towerStatusDire = towerStatusDire;
	}

	public int getTowerStatusDire(){
		return towerStatusDire;
	}

	public void setPositiveVotes(int positiveVotes){
		this.positiveVotes = positiveVotes;
	}

	public int getPositiveVotes(){
		return positiveVotes;
	}

	public void setGameMode(int gameMode){
		this.gameMode = gameMode;
	}

	public int getGameMode(){
		return gameMode;
	}

	public void setRadiantScore(int radiantScore){
		this.radiantScore = radiantScore;
	}

	public int getRadiantScore(){
		return radiantScore;
	}

	public void setRadiantTeamId(int radiantTeamId){
		this.radiantTeamId = radiantTeamId;
	}

	public int getRadiantTeamId(){
		return radiantTeamId;
	}

	public void setBarracksStatusDire(int barracksStatusDire){
		this.barracksStatusDire = barracksStatusDire;
	}

	public int getBarracksStatusDire(){
		return barracksStatusDire;
	}

	@Override
 	public String toString(){
		return 
			"DOTAMatch{" + 
			"first_blood_time = '" + firstBloodTime + '\'' + 
			",cluster = '" + cluster + '\'' + 
			",match_seq_num = '" + matchSeqNum + '\'' + 
			",flags = '" + flags + '\'' + 
			",pre_game_duration = '" + preGameDuration + '\'' + 
			",human_players = '" + humanPlayers + '\'' + 
			",barracks_status_radiant = '" + barracksStatusRadiant + '\'' + 
			",duration = '" + duration + '\'' + 
			",radiant_win = '" + radiantWin + '\'' + 
			",radiant_logo = '" + radiantLogo + '\'' + 
			",engine = '" + engine + '\'' + 
			",dire_name = '" + direName + '\'' + 
			",negative_votes = '" + negativeVotes + '\'' + 
			",radiant_captain = '" + radiantCaptain + '\'' + 
			",picks_bans = '" + picksBans + '\'' + 
			",dire_team_complete = '" + direTeamComplete + '\'' + 
			",dire_team_id = '" + direTeamId + '\'' + 
			",tower_status_radiant = '" + towerStatusRadiant + '\'' + 
			",players = '" + players + '\'' + 
			",match_id = '" + matchId + '\'' + 
			",lobby_type = '" + lobbyType + '\'' + 
			",radiant_team_complete = '" + radiantTeamComplete + '\'' + 
			",start_time = '" + startTime + '\'' + 
			",radiant_name = '" + radiantName + '\'' + 
			",leagueid = '" + leagueid + '\'' + 
			",dire_logo = '" + direLogo + '\'' + 
			",dire_score = '" + direScore + '\'' + 
			",dire_captain = '" + direCaptain + '\'' + 
			",tower_status_dire = '" + towerStatusDire + '\'' + 
			",positive_votes = '" + positiveVotes + '\'' + 
			",game_mode = '" + gameMode + '\'' + 
			",radiant_score = '" + radiantScore + '\'' + 
			",radiant_team_id = '" + radiantTeamId + '\'' + 
			",barracks_status_dire = '" + barracksStatusDire + '\'' + 
			"}";
		}
}