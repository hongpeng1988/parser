package opendota.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Player{

	@SerializedName("kills")
	private int kills;

	@SerializedName("gold_spent")
	private int goldSpent;

	@SerializedName("hero_damage")
	private int heroDamage;

	@SerializedName("backpack_1")
	private int backpack1;

	@SerializedName("backpack_2")
	private int backpack2;

	@SerializedName("denies")
	private int denies;

	@SerializedName("backpack_0")
	private int backpack0;

	@SerializedName("gold")
	private int gold;

	@SerializedName("assists")
	private int assists;

	@SerializedName("tower_damage")
	private int towerDamage;

	@SerializedName("scaled_hero_healing")
	private int scaledHeroHealing;

	@SerializedName("xp_per_min")
	private int xpPerMin;

	@SerializedName("hero_id")
	private int heroId;

	@SerializedName("deaths")
	private int deaths;

	@SerializedName("gold_per_min")
	private int goldPerMin;

	@SerializedName("level")
	private int level;

	@SerializedName("player_slot")
	private int playerSlot;

	@SerializedName("scaled_hero_damage")
	private int scaledHeroDamage;

	@SerializedName("last_hits")
	private int lastHits;

	@SerializedName("account_id")
	private int accountId;

	@SerializedName("leaver_status")
	private int leaverStatus;

	@SerializedName("scaled_tower_damage")
	private int scaledTowerDamage;

	@SerializedName("hero_healing")
	private int heroHealing;

	@SerializedName("item_5")
	private int item5;

	@SerializedName("item_4")
	private int item4;

	@SerializedName("item_3")
	private int item3;

	@SerializedName("item_2")
	private int item2;

	@SerializedName("item_1")
	private int item1;

	@SerializedName("ability_upgrades")
	private List<AbilityUpgrade> abilityUpgrades;

	@SerializedName("item_0")
	private int item0;

	public void setKills(int kills){
		this.kills = kills;
	}

	public int getKills(){
		return kills;
	}

	public void setGoldSpent(int goldSpent){
		this.goldSpent = goldSpent;
	}

	public int getGoldSpent(){
		return goldSpent;
	}

	public void setHeroDamage(int heroDamage){
		this.heroDamage = heroDamage;
	}

	public int getHeroDamage(){
		return heroDamage;
	}

	public void setBackpack1(int backpack1){
		this.backpack1 = backpack1;
	}

	public int getBackpack1(){
		return backpack1;
	}

	public void setBackpack2(int backpack2){
		this.backpack2 = backpack2;
	}

	public int getBackpack2(){
		return backpack2;
	}

	public void setDenies(int denies){
		this.denies = denies;
	}

	public int getDenies(){
		return denies;
	}

	public void setBackpack0(int backpack0){
		this.backpack0 = backpack0;
	}

	public int getBackpack0(){
		return backpack0;
	}

	public void setGold(int gold){
		this.gold = gold;
	}

	public int getGold(){
		return gold;
	}

	public void setAssists(int assists){
		this.assists = assists;
	}

	public int getAssists(){
		return assists;
	}

	public void setTowerDamage(int towerDamage){
		this.towerDamage = towerDamage;
	}

	public int getTowerDamage(){
		return towerDamage;
	}

	public void setScaledHeroHealing(int scaledHeroHealing){
		this.scaledHeroHealing = scaledHeroHealing;
	}

	public int getScaledHeroHealing(){
		return scaledHeroHealing;
	}

	public void setXpPerMin(int xpPerMin){
		this.xpPerMin = xpPerMin;
	}

	public int getXpPerMin(){
		return xpPerMin;
	}

	public void setHeroId(int heroId){
		this.heroId = heroId;
	}

	public int getHeroId(){
		return heroId;
	}

	public void setDeaths(int deaths){
		this.deaths = deaths;
	}

	public int getDeaths(){
		return deaths;
	}

	public void setGoldPerMin(int goldPerMin){
		this.goldPerMin = goldPerMin;
	}

	public int getGoldPerMin(){
		return goldPerMin;
	}

	public void setLevel(int level){
		this.level = level;
	}

	public int getLevel(){
		return level;
	}

	public void setPlayerSlot(int playerSlot){
		this.playerSlot = playerSlot;
	}

	public int getPlayerSlot(){
		return playerSlot;
	}

	public void setScaledHeroDamage(int scaledHeroDamage){
		this.scaledHeroDamage = scaledHeroDamage;
	}

	public int getScaledHeroDamage(){
		return scaledHeroDamage;
	}

	public void setLastHits(int lastHits){
		this.lastHits = lastHits;
	}

	public int getLastHits(){
		return lastHits;
	}

	public void setAccountId(int accountId){
		this.accountId = accountId;
	}

	public int getAccountId(){
		return accountId;
	}

	public void setLeaverStatus(int leaverStatus){
		this.leaverStatus = leaverStatus;
	}

	public int getLeaverStatus(){
		return leaverStatus;
	}

	public void setScaledTowerDamage(int scaledTowerDamage){
		this.scaledTowerDamage = scaledTowerDamage;
	}

	public int getScaledTowerDamage(){
		return scaledTowerDamage;
	}

	public void setHeroHealing(int heroHealing){
		this.heroHealing = heroHealing;
	}

	public int getHeroHealing(){
		return heroHealing;
	}

	public void setItem5(int item5){
		this.item5 = item5;
	}

	public int getItem5(){
		return item5;
	}

	public void setItem4(int item4){
		this.item4 = item4;
	}

	public int getItem4(){
		return item4;
	}

	public void setItem3(int item3){
		this.item3 = item3;
	}

	public int getItem3(){
		return item3;
	}

	public void setItem2(int item2){
		this.item2 = item2;
	}

	public int getItem2(){
		return item2;
	}

	public void setItem1(int item1){
		this.item1 = item1;
	}

	public int getItem1(){
		return item1;
	}

	public void setAbilityUpgrades(List<AbilityUpgrade> abilityUpgrades){
		this.abilityUpgrades = abilityUpgrades;
	}

	public List<AbilityUpgrade> getAbilityUpgrades(){
		return abilityUpgrades;
	}

	public void setItem0(int item0){
		this.item0 = item0;
	}

	public int getItem0(){
		return item0;
	}

	@Override
 	public String toString(){
		return
			"PlayersItem{" +
			"kills = '" + kills + '\'' + 
			",gold_spent = '" + goldSpent + '\'' + 
			",hero_damage = '" + heroDamage + '\'' + 
			",backpack_1 = '" + backpack1 + '\'' + 
			",backpack_2 = '" + backpack2 + '\'' + 
			",denies = '" + denies + '\'' + 
			",backpack_0 = '" + backpack0 + '\'' + 
			",gold = '" + gold + '\'' + 
			",assists = '" + assists + '\'' + 
			",tower_damage = '" + towerDamage + '\'' + 
			",scaled_hero_healing = '" + scaledHeroHealing + '\'' + 
			",xp_per_min = '" + xpPerMin + '\'' + 
			",hero_id = '" + heroId + '\'' + 
			",deaths = '" + deaths + '\'' + 
			",gold_per_min = '" + goldPerMin + '\'' + 
			",level = '" + level + '\'' + 
			",player_slot = '" + playerSlot + '\'' + 
			",scaled_hero_damage = '" + scaledHeroDamage + '\'' + 
			",last_hits = '" + lastHits + '\'' + 
			",account_id = '" + accountId + '\'' + 
			",leaver_status = '" + leaverStatus + '\'' + 
			",scaled_tower_damage = '" + scaledTowerDamage + '\'' + 
			",hero_healing = '" + heroHealing + '\'' + 
			",item_5 = '" + item5 + '\'' + 
			",item_4 = '" + item4 + '\'' + 
			",item_3 = '" + item3 + '\'' + 
			",item_2 = '" + item2 + '\'' + 
			",item_1 = '" + item1 + '\'' + 
			",ability_upgrades = '" + abilityUpgrades + '\'' + 
			",item_0 = '" + item0 + '\'' + 
			"}";
		}
}