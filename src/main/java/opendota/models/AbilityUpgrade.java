package opendota.models;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class AbilityUpgrade{

	@SerializedName("level")
	private int level;

	@SerializedName("ability")
	private int ability;

	@SerializedName("time")
	private int time;

	public void setLevel(int level){
		this.level = level;
	}

	public int getLevel(){
		return level;
	}

	public void setAbility(int ability){
		this.ability = ability;
	}

	public int getAbility(){
		return ability;
	}

	public void setTime(int time){
		this.time = time;
	}

	public int getTime(){
		return time;
	}

	@Override
 	public String toString(){
		return 
			"AbilityUpgradesItem{" + 
			"level = '" + level + '\'' + 
			",ability = '" + ability + '\'' + 
			",time = '" + time + '\'' + 
			"}";
		}
}